#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    if string.count("@") != 1:  # cuenta que en el string haya un @
        return False

    usuario, dominio = string.split("@")  # separo la parte izquierda y derecha

    if "." not in dominio:  # comprobar si no hay un punto en la parte derecha
        return False

    if not usuario or not dominio:  # si no hay ni parte izquierda o derecha
        return False

    return True


def es_entero(string):
    try:  # hacemos que se convierta a lo que queremos, si se puede, es correcto
        int(string)
        return True
    except ValueError:
        return False


def es_real(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    # hacemos una cadena de ifelse de todas las funciones
    if es_correo_electronico(string):
        return 'Es un correo electrónico.'
    elif es_entero(string):
        return 'Es un entero.'
    elif es_real(string):
        return 'Es un número real.'
    elif string == "":
        return None
    else:
        return 'No es ni un correo, ni un entero, ni un número real.'


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)


if __name__ == '__main__':
    main()
